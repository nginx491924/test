## Установка и настройка Nginx

1. Обновите список пакетов:

   ```bash
   sudo apt update
   ```

2. Установите Nginx:

   ```bash
   sudo apt install nginx -y
   ```

3. Создайте HTML файл:

   ```bash
   echo 'Hello, DevOps World!' > /var/www/html/index.html
   ```

4. Перезапустите Nginx:

   ```bash
   sudo systemctl restart nginx
   ```

Теперь веб-сервер будет доступен по адресу `localhost:80`.

## Dockerfile запуск

1. Сборка образа
	```bash
	docker build -t my-nginx-image .
	```
2. Запуск контейнера
	```bash
        docker run -d -p 80:80 --name my-nginx-container my-nginx-image
   ```
3. Проверка контейнера

	```bash
        docker ps
   ```
